package com.example.chandan.listenerapp.retrofit;

import com.example.chandan.listenerapp.model.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Scorpion .
 */
public interface RestClient {
    // User services
    @POST("users/login")
    Call<LoginResponse> login(@Body ServerRequest model);
}
