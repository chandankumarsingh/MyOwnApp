package com.example.chandan.listenerapp;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by chandan on 6/7/17.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private MainActivity mActivity;
    public ArrayList<ListModel> list;
    public ImageView imageView;
    private View.OnClickListener onDeleteClickListener;
    private View.OnClickListener onCheckClickListener;
    private ListModel mModel;
    private static final int PICKFILE_REQUEST_CODE =100;

    public ListAdapter(MainActivity mActivity, ArrayList<ListModel> list) {
        this.mActivity = mActivity;
        this.list = list;
    }

    public void setOnDeleteClickListener(View.OnClickListener onDeleteClickListener) {
        this.onDeleteClickListener = onDeleteClickListener;
    }

    public void setOnCheckClickListener(View.OnClickListener onCheckClickListener) {
        this.onCheckClickListener = onCheckClickListener;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(
                parent.getContext());
        View v = layoutInflater.inflate(R.layout.list_row, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.onBind(holder,position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public View layout;
        //        public ImageView imageView;
        public TextView rvTxtList;
        public CheckBox checkBox;
        public ViewHolder(View itemView) {
            super(itemView);
            layout = itemView;
            rvTxtList = (TextView) itemView.findViewById(R.id.txtList);
            checkBox = (CheckBox) itemView.findViewById(R.id.cbItems);
            imageView = (ImageView) itemView.findViewById(R.id.imgActionDelete);
            imageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
//            delete(getAdapterPosition());
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("*/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            mActivity.startActivityForResult(intent, PICKFILE_REQUEST_CODE);
        }

        public void onBind(final ViewHolder holder, int position) {
            final ListModel item = list.get(position);
            rvTxtList.setText( item.getName());
            holder.checkBox.setChecked(item.getSelected());
            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item.setSelected(!item.getSelected());
                }
            });
//            imageView.setOnClickListener(onDeleteClickListener);
        }


    }

    public void delete(int position) {
        list.remove(position);
        notifyDataSetChanged();
    }


}
