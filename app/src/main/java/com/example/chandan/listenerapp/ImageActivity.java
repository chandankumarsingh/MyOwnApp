package com.example.chandan.listenerapp;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ImageActivity extends AppCompatActivity {
    ImageView image;
    String imageUri;
    File mTempFile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        image = (ImageView) findViewById(R.id.imageAction);
        imageUri = getIntent().getStringExtra("URI");

        if (imageUri != null) {
            Uri fileUri = Uri.parse(imageUri);
//            image.setImageURI(fileUri);
            saveInCard(fileUri);
            Picasso.with(ImageActivity.this).load(mTempFile).noPlaceholder().centerCrop().fit()
                    .into(image);
        }
    }

    void saveInCard(Uri uri){
        String mimeType = getContentResolver().getType(uri);
        String ext = MimeTypeMap.getSingleton().getExtensionFromMimeType(mimeType);

        String sourcePath = getExternalFilesDir(null).toString();
        mTempFile = new File(sourcePath + "/" + System.currentTimeMillis() + "." + ext);
        try {
            copyFileStream(mTempFile, uri);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

        private void copyFileStream(File dest, Uri uri)
        throws IOException {
            InputStream is = null;
            OutputStream os = null;
            try {
                is = getContentResolver().openInputStream(uri);
                os = new FileOutputStream(dest);
                byte[] buffer = new byte[1024];
                int length;

                while ((length = is.read(buffer)) > 0) {
                    os.write(buffer, 0, length);

                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                is.close();
                os.close();
            }
        }

}
