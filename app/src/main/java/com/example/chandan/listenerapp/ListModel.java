package com.example.chandan.listenerapp;

/**
 * Created by chandan on 7/7/17.
 */

public class ListModel {
    String name;
    Boolean isSelected;

    public ListModel(String name, Boolean isSelected) {
        this.name = name;
        this.isSelected = isSelected;
    }

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
