package com.example.chandan.listenerapp;

import android.content.Context;
import android.content.SharedPreferences;

import static com.example.chandan.listenerapp.Constants.PREF_TOKEN;

public class PreferenceHelper {
    private static final String TAG = PreferenceHelper.class.getSimpleName();
    private static final String SHARED_PREFS_NAME = App.getInstance().getPackageName();

    private static PreferenceHelper instance;

    private SharedPreferences sharedPreferences;

    public static synchronized PreferenceHelper getInstance() {
        if (instance == null) {
            instance = new PreferenceHelper();
        }

        return instance;
    }

    private PreferenceHelper() {
        instance = this;
        sharedPreferences = App.getInstance().getSharedPreferences(SHARED_PREFS_NAME,
                Context.MODE_PRIVATE);
    }

    public void delete(String key) {
        if (sharedPreferences.contains(key)) {
            getEditor().remove(key).commit();
        }
    }

    public void save(String key, Object value) {
        SharedPreferences.Editor editor = getEditor();
        if (value instanceof Boolean) {
            editor.putBoolean(key, (Boolean) value);
        } else if (value instanceof Integer) {
            editor.putInt(key, (Integer) value);
        } else if (value instanceof Float) {
            editor.putFloat(key, (Float) value);
        } else if (value instanceof Long) {
            editor.putLong(key, (Long) value);
        } else if (value instanceof String) {
            editor.putString(key, (String) value);
        } else if (value != null) {
            throw new RuntimeException("Attempting to save non-supported preference");
        }

        editor.commit();
    }

    public <T> T get(String key) {
        return (T) sharedPreferences.getAll().get(key);
    }

    public <T> T get(String key, T defValue) {
        T returnValue = (T) sharedPreferences.getAll().get(key);
        return returnValue == null ? defValue : returnValue;
    }

    public boolean has(String key) {
        return sharedPreferences.contains(key);
    }

    private SharedPreferences.Editor getEditor() {
        return sharedPreferences.edit();
    }


    public void saveUserToken(String token) {
        save(PREF_TOKEN, token);
    }

    public String getUserToken() {
        String tokenStr = get(PREF_TOKEN);
        if (tokenStr != null) {
            return tokenStr;
        } else {
            return "";
        }
    }

}
