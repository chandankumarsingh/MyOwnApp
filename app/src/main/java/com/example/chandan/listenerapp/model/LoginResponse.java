package com.example.chandan.listenerapp.model;

/**
 */

public class LoginResponse {

    private User User;

    private String authToken;

    private boolean isNewUser = false;

    public User getUser() {
        return User;
    }

    public void setUser(User user) {
        User = user;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }


    public boolean isNewUser() {
        return isNewUser;
    }

    public void setNewUser(boolean newUser) {
        isNewUser = newUser;
    }
}
