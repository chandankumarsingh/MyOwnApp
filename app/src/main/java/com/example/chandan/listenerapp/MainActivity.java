package com.example.chandan.listenerapp;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,View.OnTouchListener{

    private RecyclerView recyclerView;
    private ListAdapter mAdapter;
    private ArrayList<ListModel> list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    public void initView() {
        recyclerView = (RecyclerView) findViewById(R.id.rvList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        list = new ArrayList<ListModel>();
        setData(list);
        mAdapter = new ListAdapter(this,list);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setOnDeleteClickListener(onDeleteClickListener);
        mAdapter.setOnCheckClickListener(onCheckClickListener);
    }

    public ArrayList setData(ArrayList<ListModel> list){

        list.add(new ListModel("chandan",false));
        list.add(new ListModel("amit",false));
        list.add(new ListModel("devam",false));
        list.add(new ListModel("shivam",false));
        list.add(new ListModel("manish",false));
        list.add(new ListModel("ankit",false));
        list.add(new ListModel("abhi",false));
        list.add(new ListModel("shailesh",false));
        list.add(new ListModel("krutik",false));
        list.add(new ListModel("prateek",false));
        list.add(new ListModel("hitesh",false));
        list.add(new ListModel("maulik",false));
        list.add(new ListModel("Abhi",false));
        list.add(new ListModel("dinesh",false));
        list.add(new ListModel("anmol",false));
        list.add(new ListModel("ruchit",false));
        list.add(new ListModel("deepak",false));
        list.add(new ListModel("ashu",false));
        list.add(new ListModel("hemu",false));
        return list;
    }

    public View.OnClickListener onDeleteClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (int) v.getTag();
            mAdapter.delete((position));


        }
    };

    public View.OnClickListener onCheckClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CheckBox cb = (CheckBox) v;
            int position = (int) v.getTag();
            mAdapter.notifyItemChanged(position);
        }
    };

    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == Activity.RESULT_OK && data != null) {
            Uri selectedImageURI = data.getData();
            Intent i = new Intent(MainActivity.this, ImageActivity.class);
            Bundle bundle = new Bundle();
            i.putExtra("URI",selectedImageURI.toString());
            startActivity(i);
        }
    }
}

