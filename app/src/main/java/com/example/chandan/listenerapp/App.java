package com.example.chandan.listenerapp;

import android.app.Application;

/**
 * Created by bhavikbansal on 7/19/17.
 */

public class App extends Application{
    private static App instance;
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static App getInstance() {
        return  instance;
    }
}
