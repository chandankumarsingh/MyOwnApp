package com.example.chandan.listenerapp.retrofit;

import com.example.chandan.listenerapp.Constants;
import com.example.chandan.listenerapp.PreferenceHelper;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {
    // No need to instantiate this class.
    private static final HttpLoggingInterceptor.Level httpLogLevel = HttpLoggingInterceptor.Level.BODY;
    private ServiceGenerator() {
    }

    /**
     * @param serviceClass
     * @param <S>
     * @return
     */
    public static <S> S createService(Class<S> serviceClass) {
        // set your desired log level
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(httpLogLevel);

        final OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
        okHttpClient.addInterceptor(httpLoggingInterceptor);
        okHttpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                String token = PreferenceHelper.getInstance().getUserToken();
                Request request = chain.request().newBuilder()
                        .header("Content-Type", "application/json")
                        .header("X-Access-Token", token)
                        .build();

                return chain.proceed(request);
            }
        });

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(Constants.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create());

        OkHttpClient client = okHttpClient.build();
        Retrofit retrofit = builder.client(client).build();
        return retrofit.create(serviceClass);
    }
}
