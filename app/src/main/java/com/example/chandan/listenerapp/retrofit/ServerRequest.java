package com.example.chandan.listenerapp.retrofit;

import com.example.chandan.listenerapp.model.User;

/**
 */

public class ServerRequest {
    private User user;
    private String token;
    private String type;
    private String mobileId;
    private String mobileType;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMobileId() {
        return mobileId;
    }

    public void setMobileId(String mobileId) {
        this.mobileId = mobileId;
    }

    public String getMobileType() {
        return mobileType;
    }

    public void setMobileType(String mobileType) {
        this.mobileType = mobileType;
    }
}
